/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Class to search urls inside the content of specific URL.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
public class Searcher implements Runnable {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(Searcher.class);

    /**
     * Define invalid holders inside links.
     */
    private static final List<String> INVALID_HOLDERS = Arrays.asList("?", "#", "mailto:", "javascript", ".png", ".jpg",
            ".gif", ".svg", ".cfm", ".pdf", ".asp", ".php", ".htm", ".js", ".xml");

    /**
     * URL to start a new search for crawling.
     */
    private URL start;

    /**
     * Processor to get the links inside the URL.
     */
    private Processor processor;

    /**
     * Constructor using start URL as parameter to start the task.
     *
     * @param start     URL to start the search the links.
     * @param processor Processor to crawl the links of the current URL.
     */
    Searcher(final URL start, final Processor processor) {
        this.start = start;
        this.processor = processor;
    }

    /**
     * Executes the search using different threads.
     */
    @Override
    public void run() {

        try {

            final long startTime = new Date().getTime();

            final StringBuilder content = readUrlContent(start);
            final Map<String, Integer> links = extractLinks(start, content.toString());

            processor.addResults(start, links);

            final long endTime = new Date().getTime();

            if (0 < links.size())
                LOG.info("Searcher found {} links in {} ms.", links.size(), endTime - startTime);

        } catch (Exception e) {

            LOG.debug("Error searching the content for the url {} - ", start, e);
        }
    }

    /**
     * Retrieve the processor instance to validate the results found by searcher run method.
     *
     * @return Processor instance given to add results found by the searcher.
     */
    Processor getProcessor() {
        return processor;
    }

    /**
     * Build the complete URL for the given link using the parent URL without anchors.
     *
     * @param url  URL of the given link.
     * @param link Link to build the complete URL for the next crawl.
     * @return URL of the given link without anchors.
     */
    private String buildLink(final URL url, String link) {

        if (link.startsWith("//"))
            link = "http:" + link;
        else if (link.startsWith("/"))
            link = "http://" + url.getHost() + link;
        else if (!link.startsWith("http"))
            link = "http://" + url.getHost() + url.getFile();

        link = validateLinkAsAnchor(link);
        link = validateLinkAsResult(link);

        return link;
    }

    /**
     * Read the content of the given URL and return it in a {@link StringBuilder} object.
     *
     * @param url {@link java.net.URL} to read the content.
     * @return A {@link StringBuilder} with the content of the given URL.
     * @throws IOException If the URL is malformed.
     */
    private StringBuilder readUrlContent(final URL url) throws IOException {

        final StringBuilder content = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(url.openStream()))) {

            String line;

            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
        }

        return content;
    }

    /**
     * Extract the links for the given URL and content.
     *
     * @param url     URL with is used for crawling.
     * @param content Content to extract the links.
     * @return A {@link Map} with the extracted links from the content and how many times was found.
     */
    private Map<String, Integer> extractLinks(final URL url, final String content) {

        final String regex = "<a\\s+href\\s*=\\s*\"?(.*?)[\"|>]";
        final Map<String, Integer> links = new TreeMap<>();

        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(content);

        while (matcher.find()) {

            final String currentMatch = matcher
                    .group(1)
                    .trim();

            final String link = buildLink(url, currentMatch);

            addNewLink(links, link);
        }

        return links;
    }

    /**
     * Add new links to the given list.
     *
     * @param links Links are found by the searcher.
     * @param link  A link is going to be added if it was not found in previous iterations, otherwise to increment counter.
     */
    private void addNewLink(final Map<String, Integer> links, final String link) {

        if (checkLink(link)) {

            Integer counter = links.get(link);
            counter = counter == null ? 1 : counter + 1;

            links.put(link, counter);
        }
    }

    /**
     * Validates if the link is not anchor, email or javascript link.
     *
     * @param link Link to validate.
     * @return A boolean representing the validation of the link.
     */
    private boolean checkLink(final String link) {

        List<String> results = INVALID_HOLDERS
                .stream()
                .filter(link.toLowerCase()::contains)
                .collect(Collectors.toList());

        return results.isEmpty();
    }

    /**
     * Validate the given link does NOT contain single quotes and is NOT a relative path.
     *
     * @param link A link to validate.
     * @return A String representing a valid result link for the search.
     */
    private String validateLinkAsResult(String link) {

        link = link.replace("'", "");
        link = link.endsWith("/") ? link.substring(0, link.length() - 1) : link;

        return link;
    }

    /**
     * Validate the given link is not an anchor.
     *
     * @param link A link to validate.
     * @return A String representing the link of specific website without anchor.
     */
    private String validateLinkAsAnchor(String link) {

        final int index = link.indexOf('#');

        if (-1 != index)
            link = link.substring(0, index);

        return link;
    }

}
