/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class to extract the URLs inside the HTML content of specific website using different threads.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Processor {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(Processor.class);

    /**
     * Define maximum number of concurrent searches.
     */
    private static final int MAX_SEARCHES = 10;

    /**
     * Cache to store URLs that have been crawled already by another thread.
     */
    private ConcurrentSkipListMap<String, Integer> cache = new ConcurrentSkipListMap<>();

    /**
     * Queue with partial results to display in the UI.
     */
    private ConcurrentLinkedQueue<String[]> model = new ConcurrentLinkedQueue<>();

    /**
     * Define if the processor instance is finished or not.
     */
    private AtomicBoolean terminated = new AtomicBoolean(false);

    /**
     * Define executor service in order to process multiple URLs at the same time.
     */
    private final ExecutorService executor;

    /**
     * Define maximum time to wait until crawler found results.
     */
    private final long maxWait;

    /**
     * Maximum number of links to search.
     */
    private final int maxLinks;

    /**
     * Define start time of the processor.
     */
    private long start;

    /**
     * Constructor of the processor to define maximum maxWait and number of results.
     *
     * @param maxWait Maximum time to wait until crawler found results.
     * @param results Maximum number of links to search.
     */
    Processor(final long maxWait, final int results) {
        this.maxWait = maxWait;
        this.maxLinks = results;
        this.executor = Executors.newFixedThreadPool(MAX_SEARCHES);
    }

    /**
     * Add the given set to the concurrent set and queues.
     *
     * @param url   Source URL of the found set.
     * @param found A Set of links found for one thread.
     */
    void addResults(final URL url, final Map<String, Integer> found) {

        if (isTerminated())
            return;

        for (Map.Entry<String, Integer> entry: found.entrySet()) {

            final String key = entry.getKey();

            try {

                final URI current = new URI(key);
                final URI uri = url.toURI();

                final boolean contains = this.cache
                        .keySet()
                        .contains(key);

                final String operation = contains ? Operation.UPDATE.name() : Operation.INSERT.name();

                if (!current.equals(uri) && !contains)
                    searchLinks(current.toURL());

                Integer counter = this.cache.getOrDefault(key, 0);
                counter = counter + entry.getValue();

                enqueueModel(key, counter, operation);
                this.cache.put(key, counter);

            } catch (Exception e) {

                LOG.warn("Error parsing {} url from {} results - ", url, key, e);
            }
        }

        terminate();
    }

    /**
     * Search the links using the given url.
     *
     * @param url URL to start the search of links using multiple threads.
     */
    void searchLinks(final URL url) {

        if (0 == start)
            start = new Date().getTime();

        if (null != url)
            executor.execute(new Searcher(url, this));
    }

    /**
     * Retrieves a partial result to display on the UI.
     *
     * @return Array of string containing the columns display on the UI table.
     */
    String[] dequeueModel() {

        if (this.model.isEmpty())
            return new String[0];
        else
            return this.model.remove();
    }

    /**
     * Retrieves the number of URLs included on the result set.
     *
     * @return A Number representing the size of the result set.
     */
    int getResultSize() {
        return this.cache.size();
    }

    /**
     * Retrieves the state of the processor. True if Processor instance is finished with the search process.
     *
     * @return A boolean representing if the search process is finished.
     */
    boolean isTerminated() {
        return terminated.get();
    }

    /**
     * Retrieves the URLs included on the result set. This method should be used for testing purposes.
     *
     * @return Map containing the result set.
     */
    ConcurrentSkipListMap<String, Integer> getResults() {
        return this.cache;
    }

    /**
     * Set the start time of the processor. This method should be used for testing purposes.
     *
     * @param start Start time that processor searches for links.
     */
    void setStart(long start) {
        this.start = start;
    }

    /**
     * Add the given result to the model.
     *
     * @param url       URL added to display in the UI.
     * @param times     Number of times the URL was found.
     * @param operation Operation that was executed on the cache. INSERT or UPDATE.
     */
    private void enqueueModel(String url, Integer times, String operation) {
        this.model.add(new String[]{url, String.valueOf(times), operation});
    }

    /**
     * Checks if the current processor instance should terminate all tasks in order to finish the executor and write
     * the file with the final records.
     */
    private void terminate() {

        final long end = new Date().getTime();

        if ((this.getResultSize() > maxLinks || (end - start >= maxWait)) && !isTerminated()) {

            terminated = new AtomicBoolean(true);

            try {

                final List<Runnable> skipped = executor.shutdownNow();
                LOG.info("Number of tasks that were not submitted {}.", skipped.size());

                executor.awaitTermination(5, TimeUnit.SECONDS);

            } catch (InterruptedException ie) {

                Thread.currentThread()
                        .interrupt();

                LOG.debug("Problem finishing running tasks in executor - ", ie);
            }

            LOG.info("Termination is executed. Final records found {} with duration {} ms.", this.getResultSize(), end - start);
        }
    }

}