/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to write the URLs and counter results into a file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Writer {

    /**
     * Define line separator value to write the file.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Filename to store the results.
     */
    private static final String FILENAME = "results.txt";

    /**
     * Define private instance to follow Singleton pattern, to ensure one thread write to file.
     */
    private static final Writer INSTANCE = new Writer();

    /**
     * Private constructor to follow Singleton pattern.
     */
    private Writer() {
        super();
    }

    /**
     * Write to a file the results of the crawler using tab as separator.
     *
     * @param results Map with results to write to a file.
     */
    synchronized void writeToFile(final Map<String, Integer> results) throws IOException {

        if (!results.isEmpty()) {

            String output = results.entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + "\t" + entry.getValue())
                    .collect(Collectors.joining(LINE_SEPARATOR));

            Files.write(
                    Paths.get(FILENAME),
                    output.getBytes());
        }
    }

    /**
     * Retrieves the Singleton instance of the writer.
     *
     * @return Singleton instance of writer.
     */
    static Writer getInstance() {
        return INSTANCE;
    }

}
