/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Class to implement swing worker in order to display partial results in JFrame.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
public class Worker extends SwingWorker<Void, Object[]> {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(Worker.class);

    /**
     * Label to display the total number of table rows.
     */
    private final JLabel total;

    /**
     * Table to display the results.
     */
    private final JTable table;

    /**
     * Processor used to search the links using the given URL.
     */
    private final Processor processor;

    /**
     * Constructor of the worker in order to update records of the JTable.
     *
     * @param total     Label to display the total number of rows in the given table.
     * @param table     Table used to display the results.
     * @param processor Processor instance used to crawl the given URL.
     */
    public Worker(final JLabel total, final JTable table, final Processor processor) {
        this.total = total;
        this.table = table;
        this.processor = processor;
    }

    /**
     * Updates the records of the JTable in the background.
     */
    @Override
    protected Void doInBackground() {

        final DefaultTableModel model = (DefaultTableModel) table.getModel();

        while (!processor.isTerminated()) {

            final String[] current = processor.dequeueModel();

            if (3 == current.length)
                publish(new Object[]{current[0], Integer.parseInt(current[1]), current[2]});
        }

        done(model);

        return null;
    }

    /**
     * Process method called after several publish calls in order to update rows in the JTable.
     *
     * @param rows Rows to add to the JTable.
     */
    @Override
    protected void process(final List<Object[]> rows) {

        final DefaultTableModel model = (DefaultTableModel) table.getModel();

        int rowCount = model.getRowCount();

        for (Object[] row : rows) {

            final Operation operation = Operation.valueOf(row[2].toString());

            if (Operation.INSERT.equals(operation))
                model.addRow(row);
            else
                updateRow(model, rowCount, row[0].toString(), (Integer) row[1]);
        }

        model.fireTableDataChanged();

        String totalText = "Partial " + model.getRowCount() + " results";

        if (processor.isTerminated())
            totalText = "Total " + model.getRowCount() + " results";

        total.setText(totalText);
    }

    /**
     * Insert a new row if the url has not been displayed by the table model. Otherwise the value of the counter
     * for the url is updated in specific row.
     *
     * @param model          Table model that stores the data.
     * @param rowCount       Number of current rows in the table.
     * @param currentKey     URL published by the background process.
     * @param currentCounter Updated counter published by the background process.
     */
    private void updateRow(final DefaultTableModel model, final int rowCount, final String currentKey,
                           final Integer currentCounter) {

        int i;

        for (i = 0; i < rowCount; i++) {

            final String modelValue = model.getValueAt(i, 0)
                    .toString();

            if (modelValue.equals(currentKey)) {
                model.setValueAt(currentCounter, i, 1);
                break;
            }
        }

        if (i == rowCount)
            LOG.warn("Error updating {} record in UI.", currentKey);
    }

    /**
     * Check if the background process should be done.
     *
     * @param model Table model that stores the data.
     */
    private void done(final DefaultTableModel model) {

        LOG.info("{} records in model and status of done - {}.", model.getRowCount(), processor.isTerminated());

        final Map<String, Integer> results = processor.getResults();

        try {

            final Writer writer = Writer.getInstance();
            writer.writeToFile(results);

        } catch (IOException e) {

            LOG.error("Error writing file with {} records.", results.size());
        }

        this.cancel(true);

        LOG.info("Canceling background process to update UI.");
    }

}
