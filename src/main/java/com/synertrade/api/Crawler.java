/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class to display the result of search, showing X websites addresses using the start URL.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
public class Crawler extends JFrame implements ActionListener {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(Crawler.class);

    /**
     * Define maximum time to wait until crawler found results.
     */
    private static final long MAX_WAIT = 60000;

    /**
     * Maximum number of links to search.
     */
    private static final int MAX_LINKS = 100000;

    /**
     * Processor used to search the links using the given URL.
     */
    private Processor processor;

    /**
     * Text field to receive the start URL.
     */
    private JTextField textField;

    /**
     * Label to display the total number of table rows.
     */
    private JLabel total;

    /**
     * Table to display the results.
     */
    private JTable table;

    /**
     * Constructor to create initial interface.
     */
    public Crawler() {

        // Define windows and UI components.
        setTitle("Crawler");
        setSize(800, 600);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // Define panel to add input components.
        JPanel startPanel = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        startPanel.setLayout(layout);

        GridBagConstraints constraints = new GridBagConstraints();

        JLabel startLabel = new JLabel("Start URL: ");
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        startPanel.add(startLabel, constraints);

        textField = new JTextField();
        textField.setPreferredSize(new Dimension(380, 20));
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        startPanel.add(textField, constraints);

        JButton button = new JButton("Start");
        button.addActionListener(this);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 2;
        constraints.gridy = 0;
        startPanel.add(button, constraints);

        total = new JLabel("0 results");
        constraints.fill = GridBagConstraints.CENTER;
        constraints.gridwidth = 3;
        constraints.gridx = 0;
        constraints.gridy = 1;
        startPanel.add(total, constraints);

        // Create table to display results.
        table = new JTable(
                new DefaultTableModel(
                        new Object[][]{},
                        new String[]{"Found URL", "Times"}) {
                    @Override
                    public Class getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return String.class;
                            case 1:
                                return Integer.class;
                            default:
                                return String.class;
                        }
                    }

                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                });

        table.getColumnModel()
                .getColumn(0)
                .setPreferredWidth(750);

        table.getColumnModel()
                .getColumn(1)
                .setPreferredWidth(50);

        table.setAutoCreateRowSorter(true);

        // Add the panel to display the results.
        JPanel resultPanel = new JPanel();

        resultPanel.setBorder(BorderFactory.createTitledBorder("Results"));
        resultPanel.setLayout(new BorderLayout());
        resultPanel.add(new JScrollPane(table), BorderLayout.CENTER);

        // Add panels to the window.
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(startPanel, BorderLayout.NORTH);
        getContentPane().add(resultPanel, BorderLayout.CENTER);
    }

    /**
     * Implements the action listener to trigger the crawling process.
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        final DefaultTableModel model = (DefaultTableModel) table.getModel();
        processor = new Processor(MAX_WAIT, MAX_LINKS);

        // Validate the model has values and clean it.
        if (0 < model.getRowCount())
            model.setRowCount(0);

        // Trigger search of links.
        searchLinks();

        // Build the swing worker to update model.
        final Worker worker = new Worker(total, table, processor);

        // Execute thread in background.
        worker.execute();
    }

    /**
     * Show the crawler interface to display results.
     *
     * @param args Arguments to show the interface.
     */
    public static void main(String... args) {

        SwingUtilities.invokeLater(() -> {

            final Crawler crawler = new Crawler();
            crawler.setVisible(true);
        });
    }

    /**
     * Validate the start URL in case is valid call the business method to start the crawling.
     */
    private void searchLinks() {

        final String startUrl = textField.getText()
                .trim();

        if (startUrl.isEmpty())
            showError("Start URL is missing or wrong.");
        else
            crawlUrl(startUrl);
    }

    /**
     * Display the error messages in the crawler user interface.
     *
     * @param message Message to display.
     */
    private void showError(final String message) {
        JOptionPane.showMessageDialog(this, message, "Crawler Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Search for links using the given URL. In case the URL could NOT be crawled the method
     * calls the show error method to inform the user.
     *
     * @param startUrl A String representing a URL to start the crawling.
     */
    private void crawlUrl(final String startUrl) {

        try {

            final URL url = new URL(startUrl);
            processor.searchLinks(url);

        } catch (MalformedURLException me) {

            LOG.error("Error processing {} - ", startUrl, me);
            showError("Error processing the given URL.");
        }
    }

}
