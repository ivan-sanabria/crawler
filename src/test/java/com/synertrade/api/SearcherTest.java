/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases for searching links in specific URL.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
class SearcherTest {


    @Test
    void given_invalid_url_not_found_links() throws MalformedURLException {

        final int expectedResults = 100;
        final URL url = new URL("http://not-existing-domain.com");
        final Processor processor = new Processor(1, expectedResults);

        final Searcher searcher = new Searcher(url, processor);
        searcher.run();

        final int results = searcher.getProcessor()
                .getResultSize();

        assertEquals(0, results);
    }

    @Test
    void given_valid_url_found_links() throws MalformedURLException {

        final int expectedResults = 100;
        final URL url = new URL("https://en.wikipedia.org/wiki/Colombia");

        final Processor processor = new Processor(5000, expectedResults);

        processor.setStart(
                new Date().getTime());

        final Searcher searcher = new Searcher(url, processor);
        searcher.run();

        final int results = searcher.getProcessor()
                .getResultSize();

        assertTrue(expectedResults <= results);
    }

    @Test
    void given_valid_url_anchor_found_links() throws MalformedURLException {

        final int expectedResults = 100;
        final URL url = new URL("https://en.wikipedia.org/wiki/Colombia#Science_and_technology");

        final Processor processor = new Processor(5000, expectedResults);

        processor.setStart(
                new Date().getTime());

        final Searcher searcher = new Searcher(url, processor);
        searcher.run();

        final int results = searcher.getProcessor()
                .getResultSize();

        assertTrue(expectedResults <= results);
    }

    @Test
    void given_valid_image_url_not_found_links() throws MalformedURLException {

        final int expectedResults = 100;
        final URL url = new URL("https://image-cdn.essentiallysports.com/wp-content/uploads/20200912202247/michael-jordan-t.jpg");
        final Processor processor = new Processor(1, expectedResults);

        final Searcher searcher = new Searcher(url, processor);
        searcher.run();

        final int results = searcher.getProcessor()
                .getResultSize();

        assertEquals(0, results);
    }

}
