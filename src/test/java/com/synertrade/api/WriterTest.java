/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases for writing content to txt file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
class WriterTest {

    /**
     * Define local directory location.
     */
    private static final String LOCAL_DIRECTORY = ".";

    /**
     * Filename to store the results.
     */
    private static final String FILENAME = "results.txt";


    @Test
    void no_file_generation_with_empty_map() throws IOException {

        final Map<String, Integer> urls = new HashMap<>();

        Writer.getInstance()
                .writeToFile(urls);

        assertTrue(fileNotFound());
    }

    @Test
    void file_generation_with_given_map() throws IOException {

        final Map<String, Integer> urls = new HashMap<>();

        urls.put("A", 1);
        urls.put("B", 2);
        urls.put("C", 1);

        Writer.getInstance()
                .writeToFile(urls);

        final File dir = new File(LOCAL_DIRECTORY);
        final String[] filenames = dir.list();

        assertNotNull(filenames);
        validateFileExist(filenames);
    }

    /**
     * Validate that CSV file is not found in the current directory.
     *
     * @return A boolean representing if csv files are found.
     */
    private static boolean fileNotFound() {

        final File dir = new File(LOCAL_DIRECTORY);

        final String[] filenames = dir.list();
        assertNotNull(filenames);

        for (String filename : filenames) {

            if (filename.equals(FILENAME))
                return false;
        }

        return true;
    }

    /**
     * Validate that the give target file exists.
     *
     * @param filenames List of csv filenames found in the root directory.
     */
    private static void validateFileExist(final String[] filenames) {

        File currentFile = null;

        for (String filename : filenames) {

            if (FILENAME.equals(filename)) {
                currentFile = new File(filename);
                assertTrue(currentFile.delete());
            }
        }

        assertNotNull(currentFile);
        assertFalse(currentFile.exists());
    }

}
