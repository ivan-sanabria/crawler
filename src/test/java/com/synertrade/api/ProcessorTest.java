/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.synertrade.api;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to manage the test case, using a specific URL and validate the processor finds 1000 URLs.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
class ProcessorTest {


    @Test
    void given_null_url_not_search_trigger() throws MalformedURLException {

        final int expectedResults = 0;

        final Processor processor = new Processor(1000, expectedResults);
        processor.searchLinks(null);

        validateResults(processor, expectedResults);
    }

    @Test
    void given_valid_url_search_urls() throws MalformedURLException {

        final int expectedResults = 1000;
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final Processor processor = new Processor(10000, expectedResults);
        processor.searchLinks(givenUrl);

        Awaitility.await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> processor.getResultSize() > expectedResults);

        validateResults(processor, expectedResults);
    }

    @Test
    void given_valid_url_search_urls_with_invalid_maximum_links() throws MalformedURLException {

        final int expectedResults = 0;
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final Processor processor = new Processor(1000, expectedResults);
        processor.searchLinks(givenUrl);

        validateResults(processor, 0);
    }

    @Test
    void given_valid_url_search_urls_with_timeout() throws MalformedURLException {

        final int expectedResults = 1000;
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final Processor processor = new Processor(100, expectedResults);
        processor.searchLinks(givenUrl);

        validateResults(processor, 0);
    }

    @Test
    void given_valid_url_add_single_result_multiple_times() throws MalformedURLException {

        final int expectedResults = 1;
        URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final ConcurrentSkipListMap<String, Integer> results = new ConcurrentSkipListMap<>();
        results.put("https://en.wikipedia.org/wiki/Germany", 1);

        final Processor processor = new Processor(1000, expectedResults);

        processor.setStart(
                new Date().getTime());

        processor.addResults(givenUrl, results);

        givenUrl = new URL("https://en.wikipedia.org/wiki/Germany");

        processor.addResults(givenUrl, results);

        validateResults(processor, expectedResults);
    }

    @Test
    void given_valid_url_add_invalid_result() throws MalformedURLException {

        final int expectedResults = 3;
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final ConcurrentSkipListMap<String, Integer> results = new ConcurrentSkipListMap<>();

        results.put("https://en.wikipedia.org/wiki/Europe", 1);
        results.put("result", 1);
        results.put("https://en.wikipedia.org/wiki/Germany", 1);

        final Processor processor = new Processor(1, expectedResults);

        processor.setStart(
                new Date().getTime());

        processor.addResults(givenUrl, results);

        validateResults(processor, expectedResults - 1);
    }

    @Test
    void given_valid_url_add_valid_results_multiple_times() throws MalformedURLException {

        final int expectedResults = 3;
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final ConcurrentSkipListMap<String, Integer> results = new ConcurrentSkipListMap<>();

        results.put("https://en.wikipedia.org/wiki/Europe", 1);
        results.put("https://en.wikipedia.org/wiki/France", 1);
        results.put("https://en.wikipedia.org/wiki/Germany", 1);

        final ConcurrentSkipListMap<String, Integer> secondResults = new ConcurrentSkipListMap<>();

        secondResults.put("https://en.wikipedia.org/wiki/Italy", 1);
        secondResults.put("https://en.wikipedia.org/wiki/France", 1);
        secondResults.put("https://en.wikipedia.org/wiki/Germany", 1);

        final Processor processor = new Processor(1000, expectedResults);

        processor.setStart(
                new Date().getTime());

        processor.addResults(givenUrl, results);
        processor.addResults(givenUrl, secondResults);

        validateResults(processor, expectedResults);
    }

    @Test
    void given_valid_url_validate_empty_dequeue_model() {

        final int expectedResults = 1;

        final Processor processor = new Processor(1000, expectedResults);

        final String[] row = processor.dequeueModel();
        assertEquals(0, row.length);
    }

    @Test
    void given_valid_url_validate_dequeue_model() throws MalformedURLException {

        final int expectedResults = 1;
        final String expectedUrl = "https://en.wikipedia.org/wiki/Germany";
        final URL givenUrl = new URL("https://en.wikipedia.org/wiki/Europe");

        final ConcurrentSkipListMap<String, Integer> results = new ConcurrentSkipListMap<>();
        results.put(expectedUrl, expectedResults);

        final Processor processor = new Processor(1000, expectedResults);

        processor.setStart(
                new Date().getTime());

        processor.addResults(givenUrl, results);

        final String[] row = processor.dequeueModel();

        assertEquals(3, row.length);
        assertEquals(expectedUrl, row[0]);
        assertEquals(String.valueOf(expectedResults), row[1]);
        assertEquals(Operation.INSERT.name(), row[2]);
    }

    /**
     * Validate the results given by the processor instance.
     *
     * @param processor       Processor which makes the crawl over given URL.
     * @param expectedResults Number of expected entries on the given results.
     */
    private void validateResults(final Processor processor, final Integer expectedResults) throws MalformedURLException {

        final ConcurrentSkipListMap<String, Integer> results = processor.getResults();

        for (String key : results.keySet()) {

            final URL url = new URL(key);
            assertNotNull(url);

            final Integer counter = results.get(key);
            assertNotEquals(0, counter);
        }

        assertTrue(expectedResults <= results.size());
    }

}
