# Crawler - Interview Question 

version 1.4.0 - 20/01/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/crawler.svg)](http://bitbucket.org/ivan-sanabria/crawler/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/crawler.svg)](http://bitbucket.org/ivan-sanabria/crawler/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_crawler)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_crawler)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_crawler&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_crawler)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_crawler)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_crawler&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_crawler)

## Introduction

Test question designed to filter java web programmers over interviews. The assignment of the test:

*" Please send the Java source code for a **crawler** which:*

- *Fetches the content for a given **start URL***
- *Extracts the **links** from the content*
- *Goes on to crawl the extracted links (back to step 1)*
- *Crawler should stop after **X** found **URLs**, maximum value of **X** is 100000, it could be increased, this is set to have a finite state of the crawler*
- *Crawler should count how often a **link** have been appeared*


The test should cover the following scenarios:

1. **Crawler** should validate the **start URL**.
2. **Crawler** should display the results on **JFrame**.

*In order to optimize partial results, please add a timeout that would stop the crawler even **X** was not reached."*

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Crawler.java**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/crawler-1.4.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Example of Display Interface

After executing the application you should be able to see:

![Initial](images/initial.png)

After typing a **valid url** and click on **start** you could see results:

![Result](images/result.png)

# Contact Information

Email: icsanabriar@googlemail.com